<?php

/**
 * call api to register a new user on xenforo
 * Go to the admin panel -> options -> user registration -> untick "Enable Email Confirmation" -> save changes
 * @param $account
 */
function xenforo_api_insert($account){
  $data = array(
    'username' => $account->name,
    'password' => $account->pass,
    'email' => $account->mail,
  );
  //  @TODO: add extra items
  //  array('timezone', 'gender', 'dob_day', 'dob_month', 'dob_year', 'ip_address')
  $request = xenforo_api_request('register', $data);

  if ($request->code == 200) {
    // @TODO: show success messages
  }
  else
  {
    // @TODO: show error messages
  }
}

/**
 * call api to modify an existing user on xenforo
 * @param $account
 */
function xenforo_api_update($account){
  $original_user_name = $account->original->name;

  $hash_key = xenforo_api_authenticate($original_user_name);
//  if the hash key is empty this means that the user is missing and attempt to create
  if (empty($hash_key)){
    xenforo_api_insert($account);
    $hash_key = xenforo_api_authenticate($original_user_name);
  }

  $data = array(
    'user' => $original_user_name,
    'hash' => "{$original_user_name}:{$hash_key}",
    'username' => $account->name,
    'email' => $account->mail
  );

  $request = xenforo_api_request('edituser', $data);

  if ($request->code == 200) {
    // @TODO: handle success messages
  }
  else
  {
    // @TODO: handle error messages
  }
}

/**
 * call api to remove an existing user from xenforo
 * @param $account
 */
function xenforo_api_delete($account){
  $hash_key = xenforo_api_authenticate($account->name);

  $data = array(
    'value' => $account->name,
    'hash' => "{$account->name}:{$hash_key}",
  );

  $request = xenforo_api_request('drupal-deleteuser', $data);

  if ($request->code == 200) {
    // @TODO: handle success messages
  }
  else
  {
    // @TODO: handle error messages
  }

}

/**
 * call api to authenticate a user on xenforo
 * @param $user
 * @return $data->hash
 */
function xenforo_api_authenticate($user) {
  $data = array(
    'username' => $user,
    'password' => md5(variable_get('xenforo_key') . $user),
  );

  $request = xenforo_api_request('drupal-authenticate', $data);
  if ($request->code == 200) {
    $data = json_decode($request->data);
    return $data->hash;
  }

}

/**
 * Api call builder method
 * @param $action, [register, edituser, drupal-authenticate, drupal-deleteuser]
 * @param $data
 * @return drupal_http_request result
 */
function xenforo_api_request($action, $data){
  $xenforo_key = variable_get('xenforo_key');
  $xenforo_domain_name = variable_get('xenforo_domain');
  $xenforo_url = variable_get('xenforo_url');

  $query = drupal_http_build_query($data);
  $url = "{$xenforo_url}/api.php?action={$action}&{$query}";
  return drupal_http_request($url);


}