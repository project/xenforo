<?php

/**
 * Callback page for xenforo login.
 */
function xenforo_login_page_callback() {
  global $cookie_domain;
  dsm($cookie_domain);

  global $user;
  $xenforo_key = variable_get('xenforo_key');
  $xenforo_domain_name = variable_get('xenforo_domain');
  $xenforo_url = variable_get('xenforo_url');
  $username = $user->name;

  # set simple md5 encryption to be used as password as a convention
  
  $md5 = md5($xenforo_key . $username);

  $url = "{$xenforo_url}/api.php?action=drupal-login&username={$username}&password={$md5}&ip_address={$xenforo_domain_name}";
  dsm($url);

  $result = drupal_http_request($url);
  $data = json_decode($result->data);

  if ($result->code == 200) {
    $status = setcookie(
      $data->cookie_name,
      $data->cookie_id,
      $data->cookie_expiration,
      $data->cookie_path,
      $xenforo_domain_name
    );

    if ($status) {
//      drupal_goto($xenforo_url, array('external' => TRUE));
    }
  }
  else{
    return $data->message;
  }


}
