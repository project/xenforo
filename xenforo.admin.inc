<?php

// Add settings form.
// Url to xenforo installation
// Key
// User / Pass to make an API request.

/**
 * Define the admin xenforo configuration form.
 */

function xenforo_configuration_form($form, $form_state){
  $form['xenforo_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Xenforo url'),
    '#description' => t('Add the full url to xenforo. (http://example.com)'),
    '#default_value' => variable_get('xenforo_url'),
    '#required' => TRUE,
  );

  $form['xenforo_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Xenforo api key'),
    '#description' => t('Add the api key that is set on xenforo api'),
    '#default_value' => variable_get('xenforo_key'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}